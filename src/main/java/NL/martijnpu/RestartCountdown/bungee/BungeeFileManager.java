package NL.martijnpu.RestartCountdown.bungee;

import NL.martijnpu.RestartCountdown.Statics;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Map;
import java.util.stream.Collectors;

import static NL.martijnpu.RestartCountdown.bungee.BungeeMessages.sendConsole;

public class BungeeFileManager {
    private static Configuration configuration;

    BungeeFileManager() {

        reloadConfig();
        if (!configuration.contains("File-Version-Do-Not-Edit")
                || !configuration.get("File-Version-Do-Not-Edit", -1).equals(Statics.CONFIG_FILE_VERSION)) {
            sendConsole("Your config file is outdated!");
            updateConfig();
            sendConsole("File successfully updated!");
        }
    }

    private void updateConfig() {
        Map<String, Object> values_old = getValues();
        values_old.put("File-Version-Do-Not-Edit", Statics.CONFIG_FILE_VERSION);


        File backupFile = new File(Main.get().getDataFolder(), "config_backup.yml");
        File configFile = new File(Main.get().getDataFolder(), "config.yml");
        int i = 1;

        while (backupFile.exists())
            backupFile = new File(NL.martijnpu.RestartCountdown.bukkit.Main.get().getDataFolder(), "config_backup_" + i++ + ".yml");

        configFile.renameTo(backupFile);
        sendConsole("Saved old config to \"" + backupFile.getName() + "\"!");

        saveDefaultConfig();
        reloadConfig();
        Map<String, Object> values_new = getValues();

        values_new.forEach((path, value) -> NL.martijnpu.RestartCountdown.bukkit.Main.get().getConfig().set(path, values_old.getOrDefault(path, value)));
        saveConfig();
        reloadConfig();
    }

    public static String getMessage(String path) {
        return configuration.getString("messages." + path, path);
    }

    /*
        Config
     */
    public static Map<String, Object> getValues() {
        return configuration.getKeys().stream().collect(Collectors.toMap(key -> key, key -> configuration.get(key), (a, b) -> b));
    }

    public static void saveDefaultConfig() {
        if (!Main.get().getDataFolder().exists())
            Main.get().getDataFolder().mkdir();

        File file = new File(Main.get().getDataFolder(), "config.yml");

        if (!file.exists()) {
            try (InputStream in = Main.get().getResourceAsStream("config.yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void reloadConfig() {
        try {
            saveDefaultConfig();
            File file = new File(Main.get().getDataFolder(), "config.yml");
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(Main.get().getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
