package NL.martijnpu.RestartCountdown.bukkit;

import NL.martijnpu.RestartCountdown.Statics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class BukkitPlayerJoin implements Listener {
    BukkitPlayerJoin() {
        Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (e.getPlayer().hasPermission("restartcountdown.restart") && (Statics.newVersion > Statics.currVersion) && Main.get().getConfig().getBoolean("update-notify", true)) {
            String text = "&6&m&l+-----------------&6&l= &eRestartCountdown &6&l=&6&m&l-----------------+&r"
                    + "\n&eNew version of Prefix available!"
                    + "\nCurrent version: &f" + Statics.currVersion
                    + "\n&eNew version: &f" + Statics.newVersion
                    + "\n&6&m&l+------------------------------------------+";
            BukkitMessages.sendMessage(e.getPlayer(), text);
        }
    }
}
