package NL.martijnpu.RestartCountdown.bukkit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static NL.martijnpu.RestartCountdown.bukkit.BukkitMessages.*;

public class CmdHandler implements CommandExecutor {
    private boolean isRunning;
    private final Main plugin;
    private int count;

    CmdHandler(Main plugin) {
        isRunning = false;
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender s, Command command, String label, String[] args) {
        Player player = (s instanceof Player) ? (Player) s : null;

        if(!s.hasPermission("restartcountdown.restart")) {
            sendMessage(player,"no-permission");
            return true;
        }

        if(args.length == 0){
            args = new String[]{"help"};
        }

        switch (args[0].toLowerCase()) {
            case "help":
                sendHelpMessage(s);
                break;

            case "start":
                if(isRunning) {
                    sendMessage(player, "countdown.already-running");
                    return true;
                }

                if(args.length == 1) {
                    count = 30;
                } else {
                    try {
                        count = Integer.parseInt(args[1]);
                    } catch (NumberFormatException ex) {
                        sendMessage(player, Main.get().getConfig().getString("messages.invalid", "invalid").replaceAll("%AMOUNT%", args[1]));
                        return true;
                    }

                    if(count < 10)
                        count = 10;
                }


                if(args.length >= 3) {
                    StringBuilder message = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        message.append(" ").append(args[i]);
                    }
                    broadcast(Main.get().getConfig().getString("messages.countdown.multiple-seconds", "countdown.multiple-seconds").replaceAll("%AMOUNT%", count + "") +
                            "\n&6&l&m+----------------------------------------+" +
                            "\n&4&4" + message.toString() +
                            "\n&6&l&m+----------------------------------------+");
                    count--;
                }

                isRunning = true;
                plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this::countdown, 20L, 20L);
                break;

            case "abort":
                if(isRunning) {
                    plugin.getServer().getScheduler().cancelTasks(plugin);
                    broadcast("countdown.aborted");
                    isRunning = false;
                } else {
                    sendMessage(player, "countdown.none");
                }
                break;

            case "reload":
                Main.get().reloadConfig();
                sendMessage(player, "reload");
                break;

            default:
                sendMessage(player, "unknown");
                break;
        }
        return true;
    }


    private void countdown() {
        if((count <= 5) && (count >= 0)) {
            clearScreen();
        }

        if((count == 120) || (count == 90) || (count == 60) ||(count == 30) || (count == 15) || ((count <= 10) && (count > 1))) {
            broadcast(Main.get().getConfig().getString("messages.countdown.multiple-seconds", "countdown.multiple-seconds").replaceAll("%AMOUNT%", count + ""));
        } else if(count == 1) {
            broadcast("countdown.single-second");
            broadcast("");
        } else if(count == 0) {
            broadcast("countdown.now");
            broadcast("countdown.now");
            broadcast("countdown.now");
        } else if(count == -1) {
            plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "restart");
            plugin.getServer().getScheduler().cancelTasks(plugin);
        }

        count--;
    }

    private static void clearScreen(){
        for (int x = 0; x < 20; x++){
            broadcast("");
        }
    }

    static void sendHelpMessage(CommandSender sender){
        String helpMessage = ""
                + "&6&l&m+----------&6&l= &eRestartCountdown &6&l=&6&l&m----------+&r\n"
                + " &6➢ &a/restartcountdown help &7- &eShow this menu\n"
                + " &6➢ &a/restartcountdown start &7- &eStart a default countdown with 30 seconds\n"
                + " &6➢ &a/restartcountdown start [seconds] [message] &7- &eStart a countdown with a custom time and message\n"
                + " &6➢ &a/restartcountdown abort &7- &eStop the current countdown\n"
                + " &6➢ &a/restartcountdown reload &7- &eReload the config\n"
                + "&r&6&l&m+----------------------------------------+";
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', helpMessage));
    }
}