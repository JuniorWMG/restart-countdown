package NL.martijnpu.RestartCountdown.bukkit;

import NL.martijnpu.RestartCountdown.Statics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        new BukkitFileManager();
        new BukkitPlayerJoin();
        getCommand("restartcountdown").setExecutor(new CmdHandler(this));
        getCommand("restartcountdown").setTabCompleter(new TabComplete());
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> Statics.checkForUpdate(Double.parseDouble(getDescription().getVersion()), false));
        BukkitMessages.sendConsole("[" + this.getDescription().getName() + "] We're up and running");
    }

    @Override
    public void onDisable() {
        BukkitMessages.sendConsole("[" + this.getDescription().getName() + "] Disabled successful");
    }
}
